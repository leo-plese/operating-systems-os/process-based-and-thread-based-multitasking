Process-based and Thread-based Multitasking. Tasks:

1 Threads and processes ("dretve.c", "dretve_TAS.c", "procesi.c")

2 Dekker's mutual exclusion algorithm for threads and processes ("Dekker.c")

Implemented in C using <pthread.h>, <signal.h>, <sys/types.h>, <sys/wait.h>, <sys/ipc.h> and <sys/shm.h> header files and __atomic_test_and_set built-in function.

My lab assignment in Operating Systems, FER, Zagreb.

Task descriptions in "TaskSpecification_ThreadsAndProcesses.pdf" and "TaskSpecification_DekkerAlgo.pdf".

Created: 2018
