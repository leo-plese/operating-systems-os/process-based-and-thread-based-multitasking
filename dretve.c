#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

int A;

void *func_dretve(void *arg)
{
	int i, a;
	int br_povecanja = *((int *) arg);
	for (i = 0; i < br_povecanja; i++) {
		A++;
		/* printf("id = %ld, A = %d\n", pthread_self(), A); */
	}
}

int main(int argc, char *argv[])
{	
	int i;
	int N = atoi(argv[1]);
	int M = atoi(argv[2]);
	pthread_t thread_id[N];

	A = 0;
	
	for (i = 0; i < N; i++) {
		if (pthread_create(&thread_id[i], NULL, func_dretve, &M) != 0) {
			perror("Greska pri stvaranju dretve!\n");
			exit(1);		
		} /* else {
			printf("Kreirana D%d.\n", i);
		}*/
	}

	for (i = 0; i < N; i++)
		pthread_join(thread_id[i], NULL);	

	printf("A = %d\n", A);
	
	return 0;
}
