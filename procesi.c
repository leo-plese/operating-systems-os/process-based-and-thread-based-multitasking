#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

int id;		//id segmenta - dijeljene memorije procesa
int *A;		//varijabla u dijeljenoj memoriji procesa

void povecaj_zajednicku_var(int br_povecanja)
{
	int i;
	for (i = 0; i < br_povecanja; i++) {
		(*A)++;
		/* printf("pid = %d, A = %d\n", getpid(), *A); */
	}
}

void brisi(int sig)
{
	shmdt((char *) A);
	shmctl(id, IPC_RMID, NULL);
	exit(0);
}

int main(int argc, char *argv[])
{
	int i;
	int N = atoi(argv[1]);
	int M = atoi(argv[2]);

	//stvoriti segment - zajednicku memoriju za procese, velicine 1 int (dovoljno za pohraniti varijablu A)
	id = shmget(IPC_PRIVATE, sizeof(int), 0600);
	if (id == -1) {
		perror("Neuspjela rezervacija zajednicke memorije!\n");
		exit(1);
	}

	//vezati segment na adresni prostor procesa
	A = (int *) shmat(id, NULL, 0);

	*A = 0;

	//brisanje memorije na prekid SIGINT (1. otpustanje segmenta od procesa + 2. unistavanje segmenta)
	sigset(SIGINT, brisi);
	
	//stvaranje N procesa
	for (i = 0; i < N; i++) {
		switch (fork()) {
			case 0:		//dijete
				povecaj_zajednicku_var(M);
				exit(0);
			case -1:	//greska
				perror("Greska pri stvaranju procesa!\n");
				exit(1);
		}
	}
	
	//cekanje roditelja da zavrse svih N djece
	while (i--)
		wait(NULL);

	printf("A = %d\n", *A);

	//na kraju pobrisati memoriju i izaci i iz glavnog procesa
	brisi(0);
	
	return 0;
}
