#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#define TAS(ZASTAVICA) __atomic_test_and_set (&ZASTAVICA, __ATOMIC_SEQ_CST)

int Di = 0;	//oznaka trenutne dretve
int A;
int M;
char ZASTAVICA = 0;	//na pocetku slobodan ulaz u kriticni odsjecak

void ulaz_kriticni_odsjecak(int d)
{	
	printf("U D%d, id = %ld\n", d, pthread_self());
	//dok je TAS(ZASTAVICA) jedan, to znaci da je druga dretva u kriticnom odsjecku
	while (TAS(ZASTAVICA) != 0);
}

void izlaz_kriticni_odsjecak(int d)
{
	printf("I D%d, id = %ld; A = %d\n", d, pthread_self(), A);
	//obrada kriticnog odsjecka je zavrsila, osloboditi ulaz u kriticni odsjecak
	ZASTAVICA = 0;
}

void *TAS_iskljucivanje(void *arg)
{
	int i;
	int dretva = Di++;
	printf("TAS D%d, id = %ld\n", dretva, pthread_self());
	for (i = 0; i < M; i++) {
		ulaz_kriticni_odsjecak(dretva);
		//kriticni odsjecak
		A++;
		sleep(1);
		izlaz_kriticni_odsjecak(dretva);
		//nekritcni odsjecak
	}
}

int main(int argc, char *argv[])
{
	int i;
	int N = atoi(argv[1]);
	M = atoi(argv[2]);
	pthread_t thread_id[N];	

	A = 0;

	for (i = 0; i < N; i++) {
		if (pthread_create(&thread_id[i], NULL, TAS_iskljucivanje, NULL) != 0) {
			perror("Greska pri stvaranju dretve!\n");
			exit(1);		
		} else {
			printf("Kreirana D%d.\n", i);		
		}
	}

	for (i = 0; i < N; i++)
		pthread_join(thread_id[i], NULL);	

	printf("A = %d\n", A);
	
	return 0;
}
