#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

int Di = 0;	//oznaka trenutne dretve
int A;
int M;
int PRAVO;
int ZASTAVICA[2];

void ulaz_kriticni_odsjecak(int d)
{	
	printf("U D%d, id = %ld\n", d, pthread_self());
	ZASTAVICA[d] = 1;
	while (ZASTAVICA[1-d] != 0) {
		if (PRAVO != d)
			ZASTAVICA[d] = 0;
		while (PRAVO != d);
		ZASTAVICA[d] = 1;	
	}
}

void izlaz_kriticni_odsjecak(int d)
{
	printf("I D%d, id = %ld; A = %d\n", d, pthread_self(), A);
	PRAVO = 1 - d;
	ZASTAVICA[d] = 0;
}

void *dekker_iskljucivanje(void *arg)
{
	int i;
	int dretva = Di++;
	printf("Dekker D%d, id = %ld\n", dretva, pthread_self());
	for (i = 0; i < M; i++) {
		ulaz_kriticni_odsjecak(dretva);
		//kriticni odsjecak
		A++;
		sleep(1);
		izlaz_kriticni_odsjecak(dretva);
		//nekritcni odsjecak
	}
}

int main(int argc, char *argv[])
{	
	int i;
	M = atoi(argv[1]);
	pthread_t thread_id[2];	

	A = 0;

	for (i = 0; i < 2; i++) {
		if (pthread_create(&thread_id[i], NULL, dekker_iskljucivanje, NULL) != 0) {
			perror("Greska pri stvaranju dretve!\n");
			exit(1);		
		} else {
			printf("Kreirana D%d.\n", i);	
		}
	}

	for (i = 0; i < 2; i++)
		pthread_join(thread_id[i], NULL);

	printf("A = %d\n", A);

	return 0;
}
